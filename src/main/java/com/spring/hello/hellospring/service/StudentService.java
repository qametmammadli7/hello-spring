package com.spring.hello.hellospring.service;

import com.spring.hello.hellospring.entity.Student;
import com.spring.hello.hellospring.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

@Service
@AllArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    @PostConstruct
    public void initialize() {
        Student student = new Student();
        student.setBirthdate(LocalDate.now());
        student.setInstitute("Harvard University");
        student.setName("Michael");
        studentRepository.save(student);
    }
}

