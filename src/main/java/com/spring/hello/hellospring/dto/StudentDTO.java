package com.spring.hello.hellospring.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class StudentDTO implements Serializable {
    private String name;
    private String institute;
    private LocalDate birthdate;
}
